<?php

/**
 * @file
 * Builds placeholder replacement tokens processed by Morpher.
 */

/**
 * Implements hook_token_info().
 */
function token_morpher_token_info() {
  $types['morpher'] = array(
    'name' => t('Morpher'),
    'description' => t('Tokens processed by Morpher.'),
  );

  $tokens['morpher']['inflect'] = array(
    'name' => 'morpher_inflect',
    'description' => t('A token value processed by %function.', array('%function' => 'morpher_inflect()')),
    'dynamic' => TRUE,
  );

  $tokens['morpher']['spell'] = array(
    'name' => 'morpher_spell',
    'description' => t('A token value processed by %function.', array('%function' => 'morpher_spell()')),
    'dynamic' => TRUE,
  );

  $tokens['morpher']['spell_ordinal'] = array(
    'name' => 'morpher_spell_ordinal',
    'description' => t('A token value processed by %function.', array('%function' => 'morpher_spell_ordinal()')),
    'dynamic' => TRUE,
  );

  $tokens['morpher']['spell_date'] = array(
    'name' => 'morpher_spell_date',
    'description' => t('A token value processed by %function.', array('%function' => 'morpher_spell_date()')),
    'dynamic' => TRUE,
  );

  return array(
    'types' => $types,
    'tokens' => $tokens,
  );
}

/**
 * Implements hook_tokens().
 */
function token_morpher_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'morpher') {
    if ($inflect_tokens = token_find_with_prefix($tokens, 'inflect')) {
      $replacements += token_morpher_generate('morpher_inflect', $inflect_tokens, $data, $options);
    }
    if ($spell_tokens = token_find_with_prefix($tokens, 'spell')) {
      $replacements += token_morpher_generate('morpher_spell', $spell_tokens, $data, $options);
    }
    if ($spell_ordinal_tokens = token_find_with_prefix($tokens, 'spell_ordinal')) {
      $replacements += token_morpher_generate('morpher_spell_ordinal', $spell_ordinal_tokens, $data, $options);
    }
    if ($spell_date_tokens = token_find_with_prefix($tokens, 'spell_date')) {
      $replacements += token_morpher_generate('morpher_spell_date', $spell_date_tokens, $data, $options);
    }
  }

  return $replacements;
}

/**
 * Generates replacement values for a list of tokens processed by Morpher.
 *
 * @param string $function
 *   The Morpher function to be called.
 * @param array $tokens
 *   An array of tokens to be replaced.
 * @param array $data
 *   (optional) An array of keyed objects.
 * @param array $options
 *   (optional) A keyed array of settings and flags to control the token
 *   replacement process.
 *
 * @return array
 *   An associative array of replacement values.
 *
 * @see token_generate()
 */
function token_morpher_generate($function, array $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  foreach ($tokens as $name => $original) {
    $args = explode('|', $name);

    // Get the token value.
    $value = token_morpher_token_value($args[0], $data, $options);
    if ($value === FALSE) {
      continue;
    }

    // By default, use the original token value.
    $replacements[$original] = $value;
    if ($value === '' || !function_exists($function)) {
      continue;
    }

    // Prepare the value for morpher_spell_date().
    if ($function === 'morpher_spell_date') {
      $value = date_create($value) ?: date_create("@$value");

      if ($value === FALSE) {
        continue;
      }
    }

    // Process the value.
    $args[0] = $value;
    $value = call_user_func_array($function, $args);
    if ($value === NULL) {
      continue;
    }

    // Use the processed value if there are no errors.
    // Otherwise, log the error if necessary.
    if (strpos($value, '#ERROR: ') === 0) {
      if (variable_get('token_morpher_log_errors', FALSE)) {
        watchdog('token_morpher', '@error<br />Function: %function<br />Parameters: <pre>@args</pre>', array(
          '@error' => $value,
          '%function' => $function,
          '@args' => print_r($args, TRUE),
        ), WATCHDOG_ERROR);
      }
    }
    else {
      $replacements[$original] = $value;
    }
  }

  return $replacements;
}

/**
 * Generates a replacement value for a given token.
 *
 * @param string $token
 *   A token to be replaced.
 * @param array $data
 *   (optional) An array of keyed objects.
 * @param array $options
 *   (optional) A keyed array of settings and flags to control the token
 *   replacement process.
 *
 * @return string|false
 *   A replacement value for the given token, or FALSE on failure.
 *
 * @see token_replace()
 * @see token_generate()
 */
function token_morpher_token_value($token, array $data = array(), array $options = array()) {
  $text_tokens = token_scan("[$token]");

  $replacements = array();
  foreach ($text_tokens as $type => $tokens) {
    $replacements += token_generate($type, $tokens, $data, $options);
  }

  return reset($replacements);
}
