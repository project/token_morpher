# Token Morpher

Allows token values to be processed by [Morpher](https://morpher.ru/php/extension).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/token_morpher).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/token_morpher).


## Requirements

This module requires the following modules:

- [Token](https://www.drupal.org/project/token)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/7/extend/installing-modules).


## Configuration

When enabled, the module adds new tokens of type `morpher`. Use them to modify
the original token values with Morpher functions.

The module has only one setting, which can be set via [Drush](https://www.drush.org):

```bash
drush vset token_morpher_log_errors TRUE
```

This allows you to log errors from Morpher functions.


## Maintainers

Andrey Tymchuk ([WalkingDexter](https://www.drupal.org/u/walkingdexter))

Ongoing development is sponsored by [Initlab](https://www.drupal.org/initlab).
